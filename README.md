# Test Studiogenesis

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Instalación

#### Clonar este proyecto en su carpeta de trabajo y abrir el directorio:

```bash
git clone https://gitlab.com/carles.mr.19/test-studiogenesis
```

#### Abre la carpeta "":

```bash
cd test-studiogenesis
```

#### Descargar las dependencias del proyecto

Como las dependencias del proyecto son manejadas por **composer**, tenemos que ejecutar el comando:
```bash
composer install
```

### ¡Importante!

Debe crear una base de datos antes de continuar con el proceso de configuración del proyecto.

#### Configuración del entorno

La configuración del entorno se realiza en el archivo **.env** pero este archivo no se puede modificar de acuerdo con las restricciones del archivo **.gitignore**, también hay un archivo de ejemplo en el proyecto **.env.example** tenemos que copiar con el siguiente comando:

```bash
cp .env.example .env
```

Luego, debemos modificar los valores de las variables de entorno para adaptar la configuración a nuestro entorno de desarrollo, como los parámetros de conexión en la base de datos.

```bash
DB_CONNECTION=mysql
DB_HOST= // Dirección IP de la base de datos
DB_PORT=3306
DB_DATABASE= // Nombre de la base de datos
DB_USERNAME= // Usuario
DB_PASSWORD= // Contraseña
```

#### Generar clave de seguridad de la aplicación

```bash
php artisan key:generate
```

#### Migrar la base de datos

El proyecto ya cuenta con los modelos, migraciones y seeders generados. Entonces, lo único que necesitamos es ejecutar la migración y ejecutar el siguiente comando:

```bash
php artisan migrate --seed
```

#### Iniciar la aplicación

```bash
php artisan serve
```

Finalmente, tendremos que ingresar la dirección "localhost: 8000" en nuestro navegador para ver la aplicación.

---

Carles Miranda Rodriguez

[Linkedin](https://www.linkedin.com/in/carles-miranda/)

---
