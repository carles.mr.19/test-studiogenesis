<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['codigo_categoria', 'nombre_categoria', 'descripcion_categoria'];

    public function productos() {
        return $this->belongsToMany('App\Producto');
    }

}
