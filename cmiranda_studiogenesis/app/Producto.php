<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = ['codigo_producto', 'nombre', 'descripcion', 'foto'];

    public function tarifas() {
        return $this->belongsToMany('App\Tarifa');
    }

    public function categorias() {
        return $this->belongsToMany('App\Categoria');
    }

}
