<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Producto;
use App\Tarifa;
use Illuminate\Http\Request;

class ProductosController extends Controller {

    //Función index - Devolvemos una array en formato JSON con todos los Productos y Tarifa
    public function index(){
        //Cogemos todos los productos
        $productos = Producto::all();

        //Cogemos todas las tarifas
        $tarifas = Tarifa::all();

        //Devolvemos una respuesta json con una array de productos y tarifas
        return response()->json(array('productos' => $productos,'tarifas' => $tarifas));
    }

}
