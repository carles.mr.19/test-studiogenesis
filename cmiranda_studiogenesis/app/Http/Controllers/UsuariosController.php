<?php

namespace App\Http\Controllers;

use Response, File, Auth;
use Illuminate\Http\Request;
use App\User;

class UsuariosController extends Controller
{

    //Función index - Recogemos todos los Usuarios y los enviamos a la vista 'usuarios.index'.
    public function index(){
        $usuarios = User::paginate(10);
        return view('usuarios.index')->with('usuarios', $usuarios);
    }

    //Función create - Devolvemos la vista 'usuarios.create'.
    public function create() {
        return view('usuarios.create');
    }

    //Función store - Recogemos todos los datos vía Request, los validamos y se crea un nuevo Usuario.
    public function store(Request $request) {
        $usuario = new User;

        //Si recibimos una imagen.
        if($request->imagen){
            //Creamos un nombre con la fecha y la extensión de la imagen original.
            $imageName = time() . '.' . $request->imagen->getClientOriginalExtension();
            $request->imagen->move(public_path('imagenes/perfil'), $imageName);
        }else{
            //Si no recibimos nada se pondrá una imagen por defecto.
            $imageName = "user.jpg";
        }

        $usuario->name = $request->nombre;
        $usuario->last_name = $request->apellido;
        $usuario->email = $request->email;
        $usuario->birth_date = $request->fecha_nacimiento;
        $usuario->photo = $imageName;
        $usuario->password = bcrypt($request->password);

        $usuario->save();

        //Una vez creado nos redirigimos a la vista 'usuarios.index' con un mensaje de confirmación.
        return redirect()->route('usuarios.index')->with('success', 'Se ha añadido el usuario correctamente');
    }

    //Función edit - Encontramos el usuario seleccionado por la ID y los enviamos a lo vista 'usuarios.edit'.
    public function edit($id) {
        $usuario = User::find($id);

        return view('usuarios.edit')->with('usuario', $usuario);
    }

    //Función update - Recogemos todos los datos del usuario, la ID del usuario a modificar y guardamos los cambios.
    public function update(Request $request, $id) {

        $usuario = User::find($id);

        //Si recibimos una nueva imagen.
        if($request->imagen){
            //Se elimina la foto que tiene el usuario actual.
            File::delete('imagenes/perfil/'.$usuario->photo);

            //Creamos una nueva imagen y la movemos hasta el directorio 'imagenes/productos'
            $imagenNueva = time().'.'.request()->imagen->getClientOriginalExtension();
            $request->imagen->move(public_path('imagenes/perfil'), $imagenNueva);

        }else{
            //Si no mantenemos el mismo nombre
            $imagenNueva = $usuario->photo;
        }

        $usuario->name = $request->nombre;
        $usuario->last_name = $request->apellido;
        $usuario->email = $request->email;
        $usuario->birth_date = $request->fecha_nacimiento;
        $usuario->photo = $imagenNueva;

        $usuario->save();

        //Una vez modificado nos redirigimos a la vista 'productos.index' con un mensaje de confirmación.
        return redirect()->route('usuarios.index')->with('success', 'Se ha modificado el usuario correctamente');
    }

    //Función destroy - Encontramos el usuario seleccionada y si existe se elimina.
    public function destroy($id) {
        $usuario = User::find($id);

        //Si intentamos eliminar el usuario logeado, nos enviara a 'usuarios.index' con un mensaje de error.
        if($usuario->id == Auth::id()){
            return redirect()->route('usuarios.index')->with('warning','No puedes eliminar al usuario logeado.');
        }else{
            $usuario->delete();
            //También eliminamos la imagen del usuario eliminado.
            File::delete('imagenes/perfil/'.$usuario->photo);
        }

        //Una vez modificado nos redirigimos a la vista 'usuarios.index' con un mensaje de confirmación.
        return redirect()->route('usuarios.index')->with('success','Se ha eliminado correctamente.');
    }





}