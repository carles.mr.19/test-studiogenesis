<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriasController extends Controller
{

    //Función index - Recogemos todas las Categorías y las enviamos a la vista 'categorias.index'.
    public function index() {
        $categorias = Categoria::paginate(10);

        return view('categorias.index')->with('categorias', $categorias);
    }

    //Función create - Devolvemos la vista 'categorias.create'.
    public function create() {
        return view('categorias.create');
    }

    //Función store - Recogemos todos los datos vía Request, los validamos y se crea una nueva categoría.
    public function store(Request $request) {

        $request->validate([
            'nombre_categoria' => 'required',
            'descripcion_categoria' => 'required',
        ]);

        $categoria = Categoria::create($request->all());
        $categoria->save();

        //Una vez creada nos redirigimos a la vista 'categorias.index' con un mensaje de confirmación.
        return redirect()->route('categorias.index')->with('success', 'Se ha añadido la categoria correctamente');
    }

    //Función edit - Encontramos la categoría seleccionada por la ID y los enviamos a la vista 'categorias.edit'.
    public function edit($id) {
        $categoria = Categoria::find($id);
        return view('categorias.edit')->with('categoria', $categoria);
    }

    //Función update - Recogemos todos los datos de la categoría, la ID de la categoría a modificar y guardamos los cambios.
    public function update(Request $request, $id) {

        $request->validate([
            'nombre_categoria' => 'required',
            'descripcion_categoria' => 'required',
        ]);

        $categoria = Categoria::find($id);

        $categoria->nombre_categoria = $request->nombre_categoria;
        $categoria->descripcion_categoria = $request->descripcion_categoria;

        $categoria->save();

        //Una vez modificado nos redirigimos a la vista 'categorias.index' con un mensaje de confirmación.
        return redirect()->route('categorias.index')->with('success', 'Se ha modificado la categoria correctamente');
    }

    //Función destroy - Encontramos la categoría seleccionada y si existe se elimina.
    public function destroy($id) {
        $categoria = Categoria::find($id);

        if($categoria) {
            $categoria->delete();
        }

        foreach($categoria->productos as $producto){
            $producto->delete();
        }

        //Una vez eliminado nos redirigimos a la vista 'categorias.index' con un mensaje de confirmación.
        return redirect()->route('categorias.index')->with('success','Se ha eliminado correctamente.');

    }

}
