<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Tarifa;
use App\Categoria;
use Carbon\Carbon;
use File;

class ProductosController extends Controller
{
    //Función index - Recogemos todos los Productos y las enviamos a la vista 'productos.index'.
    public function index() {
        $productos = Producto::paginate(10);

        return view('productos.index')->with('productos', $productos);
    }

    //Función create - Devolvemos la vista 'productos.create'.
    public function create() {
        $categorias = Categoria::all();

        return view('productos.create')->with('categorias', $categorias);
    }

    //Función store - Recogemos todos los datos vía Request, los validamos y se crea un nuevo producto.
    public function store(Request $request) {

        $request->validate([
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'precio' => 'required',
            'nombre_producto' => 'required',
            'descripcion_producto' => 'required',
        ]);

        //Se crea una nueva Tarifa
        $tarifa = new Tarifa;
        $tarifa->fecha_inicio = $request->fecha_inicio;
        $tarifa->fecha_fin = $request->fecha_fin;
        $tarifa->precio = $request->precio;

        $tarifa->save();

        //Si recibimos una imagen.
        if($request->imagen){
            //Creamos un nombre con la fecha y la extensión de la imagen.
            $imageName = time().'.'.$request->imagen->getClientOriginalExtension();
            $request->imagen->move(public_path('imagenes/productos'), $imageName);
        }else{
            //Si no recibimos nada se pondrá una imagen por defecto.
            $imageName = "producto.png";
        }

        //Creamos el producto nuevo
        $producto = new Producto;

        $producto->nombre = $request->nombre_producto;
        $producto->descripcion = $request->descripcion_producto;
        $producto->foto = $imageName;
        $producto->precio = $request->precio;

        $producto->save();

        //Asociamos el producto con la categoría seleccionada.
        $producto->categorias()->attach($request->id_categorias);

        //Asociamos el producto con la tarifa creada para este producto.
        $producto->tarifas()->attach($tarifa);

        //Una vez creado nos redirigimos a la vista 'productos.index' con un mensaje de confirmación.
        return redirect()->route('productos.index')->with('success', 'Se ha añadido la categoria correctamente');
    }

    //Función edit - Encontramos el producto seleccionado por la ID y los enviamos a la vista 'productos.edit'.
    public function edit($id) {
        $producto = Producto::find($id);
        $categorias = Categoria::all();

        //Usamos el loop para coger la ID de la Categoría asociada con el Producto seleccionado.
        foreach($producto->categorias as $pCategoria){
            $idCategoria = $pCategoria->id;
        }

        //Pasamos el producto, las categorías y la ID de la Categoría del producto seleccionado.
        return view('productos.edit')->with('producto', $producto)->with('categorias', $categorias)->with('idPrCat', $idCategoria);
    }

    //Función update - Recogemos todos los datos del producto, la ID de la categoría a modificar y guardamos los cambios.
    public function update(Request $request, $id) {

        $request->validate([
            'nombre_producto' => 'required',
            'descripcion_producto' => 'required',
        ]);

        $producto = Producto::find($id);

        //Si recibimos una nueva imagen.
        if($request->imagen){
            //Se elimina la foto que tiene el producto actual.
            File::delete('imagenes/perfil/'.$producto->foto);

            //Creamos una nueva imagen y la movemos hasta el directorio 'imagenes/productos'
            $imagenNueva = time().'.'.request()->imagen->getClientOriginalExtension();
            $request->imagen->move(public_path('imagenes/productos'), $imagenNueva);

        }else{
            //Si no mantenemos el mismo nombre
            $imagenNueva = $producto->foto;
        }

        $producto->nombre = $request->nombre_producto;
        $producto->descripcion = $request->descripcion_producto;
        $producto->foto = $imagenNueva;
        $producto->categorias()->update(['categoria_id' => $request->id_categorias]);

        $producto->save();

        //Una vez modificado  nos redirigimos a la vista 'productos.index' con un mensaje de confirmación.
        return redirect()->route('productos.index')->with('success', 'Se ha modificado la categoria correctamente');
    }

    //Función  destroy - Encontramos el producto seleccionado y si existe se elimina.
    public function destroy($id) {
        $producto = Producto::find($id);

        if($producto) {
            $producto->delete();
            //También  eliminamos la imagen del producto a eliminar
            File::delete('imagenes/productos/'.$producto->foto);
        }

        //Una vez eliminado nos redirigimos a la vista 'productos.index' con un mensaje de confirmación.
        return redirect()->route('productos.index')->with('success','Se ha eliminado correctamente.');
    }

//Con esta función recogemos las tarifas validas para el producto que nosotros seleccionemos pasando su ID por parámetro
    /*public function getTarifa($fechaActual, $id){
        $producto = Producto::find($id);

        $hey = $producto->tarifas;
        $arrayTarifasValidas = array();

        foreach($hey as $tarifa){
            $tarifaQuery = Tarifa::where('fecha_fin', '>=', $fechaActual->toDateString())->where('fecha_inicio', '<=', $fechaActual->toDateString())->where('id', '=', $tarifa->id)->get()->first();

            array_push($arrayTarifasValidas,  $tarifaQuery);

            //dd($tarifaQuery); //Saca las validas
            //dd($producto->tarifas); //Saca las tarifas de el producto de arriba
        }

        return $arrayTarifasValidas;
    }*/
}
