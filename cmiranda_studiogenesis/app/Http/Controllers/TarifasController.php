<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Tarifa;
use Illuminate\Http\Request;

class TarifasController extends Controller
{

    //Función  index - Recogemos todas las Tarifas y las enviamos a la vista 'tarifas.index'.
    public function index() {
        $productos = Producto::paginate(10);

        return view('tarifas.index')->with('productos', $productos);
    }

    //Función  create - Recogemos todos las Productos y las enviamos a la vista 'tarifas.create'.
    public function create($id){
        $producto = Producto::find($id);

        return view('tarifas.create')->with('producto', $producto);
    }

    //Función  store - Recogemos todos los datos vía Request, los validamos y se crea una nueva tarifa.
    public function store(Request $request){

        $request->validate([
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'precio' => 'required',
        ]);

        //Buscamos el id del producto al cual se le añadirá  la tarifa creada.
        $producto = Producto::find($request->id_producto);

        $tarifa = new Tarifa;

        $tarifa->fecha_inicio = $request->fecha_inicio;
        $tarifa->fecha_fin = $request->fecha_fin;
        $tarifa->precio = $request->precio;

        $tarifa->save();

        //Asociamos la tarifa con el producto seleccionado.
        $tarifa->productos()->attach($producto);

        //Una vez creado nos redirigimos a la vista 'productos.index' con un mensaje de confirmación.
        return redirect()->route('tarifas.index')->with('success','Se ha creado correctamente.');
    }

    //Función  show - Recogemos todos los Productos y las enviamos a la vista 'tarifas.show'.
    public function show($id){
        $producto = Producto::find($id);

        return view('tarifas.show')->with('producto', $producto);
    }

    //Función  destroy - Encontramos la tarifa seleccionada y si existe se elimina.
    public function destroy($id){
        $tarifa = Tarifa::find($id);

        if($tarifa) {
            $tarifa->delete();
            //Eliminados la relación que existe entre el producto y la tarifa cuando se elimina una tarifa.
            $tarifa->productos()->detach($id);
        }

        //Una vez eliminada nos redirigimos a la vista 'productos.index' con un mensaje de confirmación.
        return redirect()->route('tarifas.index')->with('success','Se ha eliminado correctamente.');
    }
}
