<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarifa extends Model
{
    protected $fillable = ['fecha_inicio', 'fecha_fin', 'precio'];

    public function productos() {
        return $this->belongsToMany('App\Producto');
    }
}
