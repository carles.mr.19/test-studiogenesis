<?php

use Illuminate\Http\Request;
Use App\Categoria;
use App\Producto;
use App\Tarifa;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('categorias', function() {
    return response()->json(Categoria::all());
});

//Route::get('productos', function() { return response()->json(Producto::all());});

Route::get('productos', 'API\ProductosController@index');

Route::get('tarifas', function() {
    return response()->json(Tarifa::all());
});

