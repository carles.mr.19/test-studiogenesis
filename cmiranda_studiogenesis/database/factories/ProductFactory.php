<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->text(20),
        'foto' => $faker->imageUrl(640, 480, 'transport', true),
        'precio' => $faker->randomNumber(2)

    ];
});
