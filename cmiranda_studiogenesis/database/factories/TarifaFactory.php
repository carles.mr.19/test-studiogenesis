<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tarifa;
use Faker\Generator as Faker;

$factory->define(Tarifa::class, function (Faker $faker) {
    return [
        'fecha_inicio' => $faker->dateTimeBetween($startDate = '-2 year', $endDate = '-1 year'),
        'fecha_fin'   => $faker->dateTimeBetween($startDate = '-6 month', $endDate = 'now'),
        'precio' => $faker->randomNumber(2)
    ];
});
