<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create();

        DB::table('users')->insert([
            'name' => 'Administrador',
            'last_name' => 'Miranda',
            'email' => 'admin@admin.com',
            'birth_date' => '1999-06-14',
            'password' => bcrypt('password'),
            'photo' => 'user.jpg'
        ]);

    }
}
