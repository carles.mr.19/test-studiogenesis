<?php

use Illuminate\Database\Seeder;
use App\Producto;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        factory(Producto::class, 10)->create()->each(function($product) {

            $categorias = factory(App\Categoria::class)->make();
            $tarifas = factory(App\Tarifa::class)->make();

            $product->categorias()->save($categorias);
            $product->tarifas()->save($tarifas);
        });

    }
}
