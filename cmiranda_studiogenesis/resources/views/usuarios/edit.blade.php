@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Actualizar Usuario</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col">
                    @if(strpos($usuario->photo,'https') !== false)
                        <img src="{{$usuario->photo}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; margin-bottom: 25px;">
                    @else
                        <img src="{{asset('imagenes/perfil/'.$usuario->photo)}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; margin-bottom: 25px;">
                    @endif
                    <h2>{{ $usuario->name ." ". $usuario->last_name }}</h2>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <form method="POST" action="{{route('usuarios.update', $usuario->id)}}" enctype="multipart/form-data">
                        @csrf @method('PATCH')

                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nombre" value="{{$usuario->name}}" required>
                        </div>

                        <div class="form-group">
                            <label for="nombre">Apellido</label>
                            <input type="text" class="form-control" name="apellido" id="nombre" value="{{$usuario->last_name}}" required>
                        </div>

                        <div class="form-group">
                            <label for="email">Correo Electrónico</label>
                            <input type="text" class="form-control" name="email" id="email" value="{{$usuario->email}}" required>
                        </div>

                        <div class="form-group">
                            <label for="fecha_nacimiento">Fecha de Nacimiento:</label>
                            <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" max="{{date('Y-m-d')}}" value="{{$usuario->birth_date}}" required>
                        </div>

                        <div class="form-group">
                            <label>Imagen de Perfil</label>
                            <input type="file" class="form-control-file" id="imagen" name="imagen">
                            <br>
                        </div>

                        <a href="{{route('usuarios.index')}}" class="btn btn-danger">Atrás</a>
                        <button type="submit" class="btn btn-primary">Actualizar Usuario</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection