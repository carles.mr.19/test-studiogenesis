@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Añadir Usuario</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('usuarios.store')}}" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" required>
                </div>

                <div class="form-group">
                    <label for="nombre">Apellido:</label>
                    <input type="text" class="form-control" name="apellido" id="apellido" required>
                </div>

                <div class="form-group">
                    <label for="email">Correo Electrónico:</label>
                    <input type="text" class="form-control" name="email" id="email" required>
                </div>

                <div class="form-group">
                    <label for="fecha_nacimiento">Fecha de Nacimiento:</label>
                    <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" max="{{date('Y-m-d')}}" required>
                </div>

                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>

                <div class="form-group">
                    <label>Imagen de Perfil</label>
                    <input type="file" class="form-control-file" id="imagen" name="imagen">
                    <br>
                </div>

                <a href="{{route('usuarios.index')}}" class="btn btn-danger">Atrás</a>
                <button type="submit" class="btn btn-primary">Añadir</button>
            </form>
        </div>
    </div>

@endsection