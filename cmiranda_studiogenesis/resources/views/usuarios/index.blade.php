@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="float-right">
            <a href="{{route('usuarios.create')}}" class="btn btn-primary">Añadir Usuario</a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de Usuarios</h1>

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                <br>
            @elseif(session()->get('warning'))
                <div class="alert alert-warning">
                    {{ session()->get('warning') }}
                </div>
                <br>
            @endif

        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Fecha Nacimiento</th>
                        <th scope="col">Correo Electrónico</th>
                        <th scope="col">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $user)
                        <tr>
                            <th>{{$user->id}}</th>
                            <th>{{$user->name}}</th>
                            <th>{{$user->last_name}}</th>
                            <th>{{$user->birth_date}}</th>
                            <th>{{$user->email}}</th>
                            <th>
                                <a href="{{route('usuarios.edit',$user->id)}}" class="float-left">
                                    <button type="button" class="btn-primary btn-sm"><i class="far fa-edit"></i></button>
                                </a>

                                <form action="{{ route('usuarios.destroy', $user->id)}}" method="POST" class="float-left">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres eliminar este usuario?')"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$usuarios->links()}}
    </div>

@endsection