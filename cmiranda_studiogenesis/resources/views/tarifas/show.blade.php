@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="float-right">
            <a href="{{route('tarifas.agregar', $producto->id)}}" class="btn btn-primary">Añadir Tarifa</a>
        </div>
        <h1 style="font-size: 2.2rem">Tarifas</h1>
        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Fecha Inicio</th>
                        <th scope="col">Fecha Fin</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($producto->tarifas as $tarifas)
                        <tr>
                            <th>{{$tarifas->fecha_inicio}}</th>
                            <th>{{$tarifas->fecha_fin}}</th>
                            <th>{{$tarifas->precio}}</th>
                            <th>
                                <form action="{{ route('tarifas.destroy', $tarifas->id)}}" method="POST" class="float-left">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres eliminar esta tarifa?')"><i class="far fa-trash-alt"></i></button>
                                </form>

                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection