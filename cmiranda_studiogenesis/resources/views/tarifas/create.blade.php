@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Añadir Tarifa</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('tarifas.store')}}" enctype="multipart/form-data">
                @csrf

                <label>Tarifa</label>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Fecha Inicio</label>
                        <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" required>
                    </div>
                    <div class="col-md-4">
                        <label>Fecha Fin</label>
                        <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" required>
                    </div>
                    <div class="col-md-4">
                        <label>Precio</label>
                        <input type="text" class="form-control" name="precio" id="precio" required>
                    </div>
                </div>

                <input type="hidden" value="{{$producto->id}}" name="id_producto" id="id_producto">

                <a href="{{route('tarifas.index')}}" class="btn btn-danger">Atrás</a>
                <button type="submit" class="btn btn-primary">Añadir</button>
            </form>
        </div>
    </div>

@endsection