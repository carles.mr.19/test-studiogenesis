@extends('layouts.app')

@section('content')

    <div class="container">
        <h1 style="font-size: 2.2rem">Tarifas</h1>

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            <br>
        @endif

        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Categorías</th>
                        <th scope="col">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($productos as $producto)
                        <tr>
                            <th>{{$producto->id}}</th>
                            <th>{{$producto->nombre}}</th>
                            <th>{{$producto->descripcion}}</th>
                            <th>@foreach($producto->categorias as $categorias) {{$categorias->nombre_categoria }}@endforeach</th>
                        <!--<th>@foreach($producto->tarifas as $tarifa) {{$tarifa->precio}} @endforeach</th>-->
                            <th>{{$producto->precio}}</th>
                            <th>
                                <a href="{{route('tarifas.show',$producto->id)}}" class="float-left">
                                    <button type="button" class="btn-primary btn-sm"><i class="fas fa-eye"></i></button>
                                </a>

                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        {{$productos->links()}}
    </div>

@endsection