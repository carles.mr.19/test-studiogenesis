@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="float-right">
            <a href="{{route('categorias.create')}}" class="btn btn-primary">Añadir Categoría</a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de Categorías</h1>

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            <br>
        @endif

        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categorias as $categoria)
                        <tr>
                            <th>{{$categoria->id}}</th>
                            <th>{{$categoria->nombre_categoria}}</th>
                            <th>{{$categoria->descripcion_categoria}}</th>
                            <th>
                                <a href="{{route('categorias.edit',$categoria->id)}}" class="float-left">
                                    <button type="button" class="btn-primary btn-sm"><i class="far fa-edit"></i></button>
                                </a>
                                <form action="{{ route('categorias.destroy', $categoria->id)}}" method="POST" class="float-left">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres eliminar esta categoría?')"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$categorias->links()}}
    </div>

@endsection