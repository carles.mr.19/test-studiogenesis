@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Actualizar Categoría</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('categorias.update', $categoria->id)}}">
                @csrf @method('PATCH')

                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" name="nombre_categoria" id="nombre_categoria" value="{{$categoria->nombre_categoria}}" required>
                </div>

                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <textarea class="form-control" id="descripcion_categoria" name="descripcion_categoria" rows="3" required>{{$categoria->descripcion_categoria}}</textarea>
                </div>

                <a href="{{route('categorias.index')}}" class="btn btn-danger">Atrás</a>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </form>
        </div>
    </div>

@endsection