@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Añadir Categoría</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('categorias.store')}}">
                @csrf

                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" name="nombre_categoria" id="nombre_categoria" required>
                </div>

                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <textarea class="form-control" id="descripcion_categoria" name="descripcion_categoria" rows="3" required></textarea>
                </div>

                <a href="{{route('usuarios.index')}}" class="btn btn-danger">Atrás</a>
                <button type="submit" class="btn btn-primary">Añadir</button>
            </form>
        </div>
    </div>

@endsection