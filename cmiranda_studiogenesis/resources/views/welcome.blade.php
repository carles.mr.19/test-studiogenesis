<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Carles Miranda</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>

<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Inicio</a> @else
                <a href="{{ route('login') }}">Iniciar sesión</a> @if (Route::has('register'))
                    <a href="{{ route('register') }}">Registrar</a> @endif @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md"> Test Studiogenesis </div>
        <div class="subtitle m-b-md"> Creado por Carles Miranda </div>

        <div class="links">
            <a href="https://www.linkedin.com/in/carles-miranda/">Linkedin</a>
            <a href="https://gitlab.com/carles.mr.19">GitLab</a>
        </div>

        <h1><a href="{{route('home')}}"> Ir a la APP </a></h1>
    </div>

</div>
</body>

</html>