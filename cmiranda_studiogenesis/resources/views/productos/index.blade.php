@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="float-right">
            <a href="{{route('productos.create')}}" class="btn btn-primary">Añadir Producto</a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de Productos</h1>

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            <br>
        @endif

        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Categorías</th>
                        <th scope="col">Tarifa</th>
                        <th scope="col">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($productos as $producto)
                        <tr>
                            <th>{{$producto->id}}</th>
                            <th>{{$producto->nombre}}</th>
                            <th>{{$producto->descripcion}}</th>
                            <th> @foreach($producto->categorias as $categorias) {{$categorias->nombre_categoria }}@endforeach</th>
                            <th> {{$producto->precio}}</th>
                            <th>
                                <a href="{{route('productos.edit',$producto->id)}}" class="float-left">
                                    <button type="button" class="btn-primary btn-sm"><i class="far fa-edit"></i></button>
                                </a>

                                <form action="{{route('productos.destroy',$producto->id)}}" method="POST" class="float-left">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres eliminar el producto?')"><i class="far fa-trash-alt"></i></button>
                                </form>
                                <a href="{{route('tarifas.show',$producto->id)}}" class="float-left">
                                    <button type="button" class="btn-success btn-sm"><i class="fas fa-dollar-sign"></i></button>
                                </a>
                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$productos->links()}}
    </div>

@endsection