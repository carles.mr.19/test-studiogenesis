@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Añadir Producto</div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{{route('productos.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="nombre_producto">Nombre</label>
                    <input type="text" class="form-control" name="nombre_producto" id="nombre_producto" required>
                </div>

                <div class="form-group">
                    <label for="descripcion_producto">Descripción</label>
                    <textarea class="form-control" id="descripcion_producto" name="descripcion_producto" rows="3" required></textarea>
                </div>

                <div class="form-group">
                    <label for="id_categorias">Categoría</label>
                    <select class="form-control" name="id_categorias" id="id_categorias" required>
                        @foreach ($categorias as $categoria)
                            <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria }}</option>
                        @endforeach
                    </select>
                </div>

                <label>Tarifa</label>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Fecha Inicio</label>
                        <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" required>
                    </div>
                    <div class="col-md-4">
                        <label>Fecha Fin</label>
                        <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" required>
                    </div>
                    <div class="col-md-4">
                        <label>Precio</label>
                        <input type="text" class="form-control" name="precio" id="precio" required>
                    </div>
                </div>

                <div class="form-group">
                    <label>Imagen de Producto</label>
                    <input type="file" class="form-control-file" id="imagen" name="imagen">
                    <br>
                </div>

                <a href="{{route('productos.index')}}" class="btn btn-danger">Atrás</a>
                <button type="submit" class="btn btn-primary">Añadir</button>
            </form>
        </div>
    </div>
@endsection