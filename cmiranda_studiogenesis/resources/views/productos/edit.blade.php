@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Actualizar Producto</div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col">
                    @if(strpos($producto->foto,'https') !== false)
                        <img src="{{$producto->foto}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; margin-bottom: 25px;">
                    @else
                        <img src="{{asset('imagenes/productos/'.$producto->foto)}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; margin-bottom: 25px;">
                    @endif
                    <h2>{{ $producto->nombre}}</h2>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <form method="POST" action="{{route('productos.update', $producto->id)}}">
                        @csrf @method('PATCH')

                        <div class="form-group">
                            <label for="nombre_producto">Nombre</label>
                            <input type="text" class="form-control" name="nombre_producto" id="nombre_producto" value="{{$producto->nombre}}" required>
                        </div>

                        <div class="form-group">
                            <label for="descripcion_producto">Descripción</label>
                            <textarea class="form-control" id="descripcion_producto" name="descripcion_producto" rows="3" required>{{$producto->descripcion}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="id_categorias">Categorías</label>

                            <select class="form-control" name="id_categorias" id="id_categorias" required>
                                @foreach($categorias as $categoria)
                                    @if($categoria->id == $idPrCat)
                                        <option value="{{ $categoria->id }}" selected> {{$categoria->nombre_categoria}} </option>
                                    @else
                                        <option value="{{ $categoria->id }}"> {{$categoria->nombre_categoria}} </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Imagen de Producto</label>
                            <input type="file" class="form-control-file" id="imagen" name="imagen">
                            <br>
                        </div>

                        <a href="{{route('productos.index')}}" class="btn btn-danger">Atrás</a>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection