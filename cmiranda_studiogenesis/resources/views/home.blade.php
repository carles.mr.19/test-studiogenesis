@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h1> Test Studiogenesis </h1>
                <a href="{{route('usuarios.index')}}">
                    <button type="button" class="btn btn-lg btn-primary">Usuarios</button>
                </a>
                <a href="{{route('categorias.index')}}">
                    <button type="button" class="btn btn-lg btn-primary">Categorías</button>
                </a>
                <a href="{{route('productos.index')}}">
                    <button type="button" class="btn btn-lg btn-primary">Productos</button>
                </a>
                <a href="{{route('tarifas.index')}}">
                    <button type="button" class="btn btn-lg btn-primary">Tarifas</button>
                </a>
            </div>
        </div>
    </div>

@endsection